#include "ParticleSystem.h"

ParticleSystem::ParticleSystem()
{
    
}


//--------------------------------------------------------------
ParticleSystem::~ParticleSystem()
{
    
}


//--------------------------------------------------------------
void ParticleSystem::setup()
{
    pos.set(ofGetWidth()/2, ofGetHeight()/2);
    vel.set(ofRandom(-5, 5), ofRandom(-5,5));
    gravity.set(0.0f, 0.0f);
    radius = ofRandom(1,30);
    
    frc   = ofPoint(0,0,0);
    drag  = ofRandom(0.97, 0.99);

}


//--------------------------------------------------------------
void ParticleSystem::update()
{
    if(bAttract)
    {
        ofPoint attractPt(ofGetMouseX(), ofGetMouseY());
        frc = attractPt - pos; // we get the attraction force/vector by looking at the center pos relative to our pos
        frc.normalize(); //by normalizing we disregard how close the particle is to the attraction point
        vel *= drag; //apply drag
        vel += frc * 1.2; //apply force
    }
    else
    {
        ofPoint attractPt(ofGetMouseX(), ofGetMouseY());
        frc = attractPt-pos;
        
        //let get the distance and only repel points close to the mouse
        float dist = frc.length();
        frc.normalize();
        
        vel *= drag;
        
        if( dist < 150 )
        {
            vel += -frc * 0.6; //notice the frc is negative
        }else
        {
            //if the particles are not close to us, lets add a little bit of random movement using noise. this is where uniqueVal comes in handy.
            frc.x = ofSignedNoise(1000, pos.y * 0.01, ofGetElapsedTimef()*0.2);
            frc.y = ofSignedNoise(-1000, pos.x * 0.01, ofGetElapsedTimef()*0.2);
            vel += frc * 0.04;
        }
    }

    pos += vel;
    checkBorder();
}


//--------------------------------------------------------------
void ParticleSystem::draw()
{
    ofDrawCircle(pos.x, pos.y, radius);
}


//--------------------------------------------------------------
void ParticleSystem::checkBorder()
{
    if(pos.x < radius || pos.x > ofGetWidth() - radius)
    {
        vel.x *= -1;
    }
    if(pos.y < radius || pos.y > ofGetHeight() - radius)
    {
        vel.y *= -1;
    }
}


//--------------------------------------------------------------
void ParticleSystem::setAttractPoints( vector <ofPoint> *attract )
{
    attractPoints = attract;
}


//--------------------------------------------------------------
void ParticleSystem::addParticles(float force)
{
    
}


//--------------------------------------------------------------
void ParticleSystem::setColor(ofColor col)
{
    
}
