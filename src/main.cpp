#include "ofMain.h"
#include "ofApp.h"

int main( )
{
    ofGLFWWindowSettings settings;
    
    settings.decorated = true;
    settings.windowMode = OF_WINDOW;
    
    ofCreateWindow(settings);
    
	ofRunApp(new ofApp());

}
