#include "ofApp.h"


void ofApp::setup()
{
    serial.listDevices();
    serial.setup(0, 9600);
    
    ofSetFrameRate(60);
    ofSetVerticalSync(true);
    ofSetBackgroundColor(0);
    ofSetCircleResolution(360);
    
    idle = new Idle();
    idle->setup();
    
    detect = new Detection();
    detect->setup();
    
    mode = IDLE;

    for(int i = 0; i < nBall; i++)
    {
        ParticleSystem p;
        p.setup();
        particles.push_back(p);
    }
}


//--------------------------------------------------------------
void ofApp::update()
{
    ofSetWindowTitle(ofToString((int)ofGetFrameRate()));
    detect->update();
        
    for(int i = 0; i < particles.size(); i++)
    {
        particles[i].update();
    }
    

    if(serial.available() && bReceiveFromArduino)
    {
        unsigned char bytesReturned[3];
        
        // This reads the data now that arduino is sending a response,
        serial.readBytes(bytesReturned, 4);
        
        string serialData = (char*) bytesReturned;
        forceValue = ofToInt(serialData);
        cout << forceValue << endl;
        
        // puliamo la seriale da eventuali sporcizie
        serial.flush();
    
    }
}


//--------------------------------------------------------------
void ofApp::draw()
{
    setState(mode);
}


//--------------------------------------------------------------
void ofApp::setState(StateMode m)
{
    switch (m)
    {
        case IDLE:
            bReceiveFromArduino = false;
            idle->draw();
            break;
            
        case ENGANGEMENT:
            detect->draw();
            break;
            
        case VIDEO:
            break;
            
        case PLAY:
            bReceiveFromArduino = true;
            drawParticles();
            break;
            
        default:
            break;
    }
}


//--------------------------------------------------------------
void ofApp::drawParticles()
{
    for(int i = 0; i < particles.size(); i++)
    {
        particles[i].draw();
        
        if(forceValue > 40)
            particles[i].bAttract = true;
        else
            particles[i].bAttract = false;


    }
}


//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    if(key == OF_KEY_UP)
    {
        ParticleSystem p;
        p.setup();
        particles.push_back(p);
        
        cout << particles.size() << endl;
    }
    else if(key == OF_KEY_DOWN)
    {
        
    }
    else if( key == 'a')
    {
        for(int i = 0; i < particles.size(); i++)
        {
            particles[i].bAttract = !particles[i].bAttract;
        }
    }
        
}


//--------------------------------------------------------------
void ofApp::keyReleased(int key)
{
    switch (key)
    {
        case '1':
            mode = IDLE;
            break;
            
        case '2':
            mode = ENGANGEMENT;
            break;
            
        case '3':
            mode = VIDEO;
            break;
            
        case '4':
            mode = PLAY;
            break;
            
        default:
            break;
    }

}


//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}


//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}


//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}


//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}


//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
