#pragma once
#include "ofMain.h"

class ParticleSystem
{
    
public:
    
    ParticleSystem();
    ~ParticleSystem();
    void setup();
    void update();
    void draw();
    void addParticles(float force);
    void setColor(ofColor col);
    void checkBorder();
    
    void setAttractPoints( vector <ofPoint> *attract );
    vector <ofPoint> *attractPoints;
    ofPoint frc;
    float drag;
    bool bAttract;
    
    ofPoint pos;
    ofPoint vel;
    ofPoint gravity;
    ofPoint acceleration;
    float radius;
    
};
